import { getTokenMap } from "./plugin/extractTokensFromVariables";

type TokenMapMessage = {
	type: "token-map-message";
	payload: ReturnType<typeof getTokenMap>;
};

export type PluginMessage = TokenMapMessage;

type ClosePluginMessage = {
	type: "close-plugin";
};

type NotificationMessage = {
	type: "notify";
	payload: string;
};

export type UIMessage = ClosePluginMessage | NotificationMessage;
