import type { PluginMessage, UIMessage } from "../message";
import { getTokenMap } from "./extractTokensFromVariables";

figma.showUI(__html__, {
	themeColors: true,
	visible: false,
});

const postMessage = (message: PluginMessage) =>
	figma.ui.postMessage({
		payload: JSON.stringify(message.payload),
		type: message.type,
	});

const handleMessage = (message: UIMessage) => {
	if (typeof message === "object" && "type" in message) {
		switch (message.type) {
			case "close-plugin":
				figma.closePlugin();
				break;
			case "notify":
				figma.notify(message.payload);
				break;
		}
	}
};
figma.ui.onmessage = handleMessage;

postMessage({ payload: getTokenMap(), type: "token-map-message" });
