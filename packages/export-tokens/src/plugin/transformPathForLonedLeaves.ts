import { isEqual } from "lodash/fp";

const theresAValueUpOf = <T>(
	path: string[],
	array: { path: string[]; value?: T }[],
): number => {
	let tokenUp = array.find((token) => isEqual(token.path, path.slice(0, -1)));
	let numberOfLevels = 0;
	while (tokenUp !== undefined && tokenUp?.value !== undefined) {
		numberOfLevels++;
		tokenUp = array.find((token) =>
			isEqual(token.path, tokenUp?.path.slice(0, -1)),
		);
	}
	return numberOfLevels;
};

const pathTransform = (path: string[], numberOfLevels: number) => [
	...path.slice(0, -1 - numberOfLevels),
	path
		.slice(-1 - numberOfLevels, path.length)
		.reduce((acc, pathPath) => acc + "." + pathPath),
];

type ValuedPath<T> = {
	path: string[];
	value: T;
};

/**
 * Transforms path for value that are down the tree with other values above.
 * E.g. :
 * - Color/Button/Background #345673
 *  	- Color/Button/Background/Hovered (#344050) -> Transformed to Color/Button/Background.Hovered
 * Otherwise style dictionnary does not support tokens being represented as both a group and a value in JSON.
 */
export const transformPathForLonedLeaves = <T, K extends ValuedPath<T>>(
	item: K,
	array: K[],
): K => {
	const numberOfLevelsUp = theresAValueUpOf(item.path, array);
	return numberOfLevelsUp !== 0
		? {
				...item,
				path: pathTransform(item.path, numberOfLevelsUp),
				value: item.value,
			}
		: item;
};
