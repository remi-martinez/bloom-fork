import { expect, it } from "vitest";
import { transformPathForLonedLeaves } from "./transformPathForLonedLeaves";

it("transforms multiple nesting levels", () => {
	const tokens = [
		{ path: ["color", "bg", "surface", "hover"], value: 0 },
		{ path: ["color", "bg", "surface"], value: 1 },
		{ path: ["color", "bg"], value: 2 },
	];
	const result = transformPathForLonedLeaves(tokens[0], tokens);
	expect(result.path).toEqual(["color", "bg.surface.hover"]);
});

it("transforms one nesting levels", () => {
	const tokens = [
		{ path: ["color", "bg", "surface"], value: 1 },
		{ path: ["color", "bg"], value: 2 },
	];
	const result = transformPathForLonedLeaves(tokens[0], tokens);
	expect(result.path).toEqual(["color", "bg.surface"]);
});

it("does nothing for no nesting", () => {
	const tokens = [
		{ path: ["color", "bg", "surface"], value: 1 },
		{ path: ["color", "bg"], value: 2 },
		{ path: ["color", "black"], value: 2 },
	];
	const result = transformPathForLonedLeaves(tokens[2], tokens);
	expect(result).toEqual({ path: ["color", "black"], value: 2 });
});
