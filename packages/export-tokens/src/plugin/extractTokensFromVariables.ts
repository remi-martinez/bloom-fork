import { setWith } from "lodash/fp";
import { transformPathForLonedLeaves } from "./transformPathForLonedLeaves";

type TokenOrNestedTokens<T> = T | { [key: string]: TokenOrNestedTokens<T> };

type EnrichedVariable = {
	collection: VariableCollection;
	id: string;
	mode: {
		modeId: string;
		name: string;
	};
	name: string;
	value: VariableValue;
	path: string[];
};

export const getTokenMap = () =>
	figma.variables
		.getLocalVariables()
		.flatMap(enrichVariables)
		.map((item, _idx, array) => transformPathForLonedLeaves(item, array))
		.reduce((tokens, variable, _idx, initialArray) => {
			let value = variable.value;
			if (isAlias(variable.value)) {
				const id = variable.value.id;
				const ref = findOrThrow(
					initialArray,
					(_variable) => _variable.id === id
				);
				value = createAlias(ref);
			}
			return setWith(
				Object,
				variable.path.map((part) => part.toLowerCase()),
				{ value },
				tokens
			);
		}, {} as { [path: string]: TokenOrNestedTokens<{ value: VariableValue }> });

const enrichVariables = (variable: Variable): EnrichedVariable[] =>
	Object.entries(variable.valuesByMode).map(([id, value]) => {
		const collection = findOrThrow(
			figma.variables.getLocalVariableCollections(),
			(collection) => collection.id === variable.variableCollectionId
		);
		const mode = findOrThrow(collection.modes, (mode) => mode.modeId === id);
		// XXX: Wtf does this do?
		let path = variable.name.split("/");
		const theresMultipleModes = collection.modes.length > 1;
		if (theresMultipleModes) {
			path = [mode?.name, ...path];
		}
		// Add the name of the collection before
		path = [...collection.name.split("/"), ...path];

		return {
			collection,
			id: variable.id,
			mode,
			name: variable.name,
			value,
			path,
		};
	});

// XXX: slice 2 removes theme and layer prefix e.g. (primitives.loccitane)
const createAlias = (ref: { path: string[] }): VariableValue =>
	`{${ref?.path.slice(2).join(".").toLowerCase().replace(" ", ".")}}`;

const findOrThrow = <T>(array: T[], predicate: (arg: T) => boolean) => {
	const value = array.find(predicate);
	if (!value) {
		throw new Error(`No value found in array`);
	}
	return value;
};

const isAlias = (value: VariableValue): value is VariableAlias =>
	typeof value === "object" && "type" in value;
