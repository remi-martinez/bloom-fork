import { UIMessage } from "../message";

export const messagePlugin = (message: UIMessage) =>
	parent.postMessage({ pluginMessage: message }, "*");
