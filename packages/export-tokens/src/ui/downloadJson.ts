import { isEmpty } from "lodash/fp";
import { messagePlugin } from "./message";

const notify = (message: string) => {
	messagePlugin({ type: "notify", payload: message });
};

export const downloadJson = <T>(link: HTMLAnchorElement, data: T) => {
	// if no tokens are present
	if (isEmpty(data)) {
		notify("⛔️ No design token detected!");
		// abort
		return;
	}
	// try to export tokens
	try {
		link.href = `data:application/tokens+json;charset=utf-8,${encodeURIComponent(
			JSON.stringify(data, null, `\t`)
		)}`;
		// Programmatically trigger a click on the anchor element
		link.click();
		// send success message
		notify("🎉 Design token export successful!");
	} catch (error) {
		// send success message
		notify("⛔️ Design token failed!");

		// log error
		console.error("Export error: ", error);
	}
};
