import { useEffect, useRef } from "react";
import { createRoot } from "react-dom/client";
import { downloadJson } from "./downloadJson";
import { PluginMessage } from "../message";
import { messagePlugin } from "./message";

const listenMessages = (listener: (message: PluginMessage) => void) => {
	const oldOnMessage = window.onmessage;
	window.onmessage = (windowMessage) => {
		const message = windowMessage.data.pluginMessage;
		if (message) {
			listener({
				payload: JSON.parse(message.payload),
				type: message.type,
			});
		}
		if (oldOnMessage) {
			oldOnMessage.bind(window)(windowMessage);
		}
	};
};

const settings = {
	filename: "tokens.json",
};

const PluginUi = () => {
	const downloadLinkRef = useRef<HTMLAnchorElement>(null);

	useEffect(() => {
		listenMessages((message) => {
			switch (message.type) {
				case "token-map-message":
					if (downloadLinkRef.current) {
						downloadJson(downloadLinkRef.current, message.payload);
						messagePlugin({ type: "close-plugin" });
					}
			}
		});
	}, []);

	return (
		<a
			ref={downloadLinkRef}
			download={settings.filename}
			title={settings.filename}
		></a>
	);
};

const root = createRoot(document.getElementById("root")!);
root.render(<PluginUi />);
