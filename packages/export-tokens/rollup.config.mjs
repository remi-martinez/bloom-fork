import typescript from "@rollup/plugin-typescript";
import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import { defineConfig } from "rollup";

export default defineConfig({
	plugins: [typescript(), commonjs(), nodeResolve()],
	input: "./src/plugin/index.ts",
	output: { file: "./dist/plugin.js" },
});
