# @bloom-kit/tokens

## 0.1.0-next.14

### Patch Changes

- Rename property

## 0.1.0-next.13

### Patch Changes

- Wishlist

## 0.1.0-next.12

### Patch Changes

- 84dbc94: Add Feedback Message Component

## 0.1.0-next.11

### Patch Changes

- Color changes

## 0.1.0-next.10

### Patch Changes

- c26a609: style: new melvita colors

## 0.1.0-next.9

### Patch Changes

- button and card improvements

## 0.1.0-next.8

### Patch Changes

- b964e67: feat(tokens): add all tertiary button tokens

## 0.1.0-next.7

### Patch Changes

- fd7a4e8: fix(components): button sizes, card link property inheritance

## 0.1.0-next.6

### Patch Changes

- d17974a: fix(components): typo in tertiary

## 0.1.0-next.5

### Patch Changes

- cb42307: feat(components): add terciary button

## 0.1.0-next.4

### Patch Changes

- b72434b: add clickable/focusable link card

## 0.1.0-next.3

### Minor Changes

- ae70cf8: add "font-weight" token

## 0.0.1-next.2

### Patch Changes

- 117967e: Wrap fonts with quotes

## 0.0.1-next.1

### Patch Changes

- 6e67417: Add scss/map-flat output

## 0.0.1-next.0

### Patch Changes

- 5dfdbb9: Initial test release
