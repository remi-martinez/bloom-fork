import { buildCssVariablesFor } from "./token-builder.mjs";
import { rewriteRawTokens } from "./rewriteRawTokens.mjs";
import tokens from "../tokens.json" assert { type: "json" };
import { cpSync } from "fs";
import { join } from "path";

const layers = ["primitives", "semantic", "components"];
const themes = ["loccitane", "melvita", "erborian", "erborian-us"];
const buildConfig = {
	outputDirectory: "./dist/",
};

for (const theme of themes) {
	rewriteRawTokens(theme, tokens, layers);
	buildCssVariablesFor(theme, layers, buildConfig);
}

cpSync("package.json", join(buildConfig.outputDirectory, "package.json"));
