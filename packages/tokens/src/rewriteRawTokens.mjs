import { writeFileSync } from "fs";

const removePrefix = (data, prefix) => {
	const newData = {};
	for (const [key, value] of Object.entries(data)) {
		if (typeof value === "string") {
			const newValue = value
				.replace(`${prefix}.`, "")
				.replace(`${prefix}.`, "");
			newData[key] = newValue;
		} else if (typeof value === "object") {
			newData[key] = removePrefix(value, prefix);
		} else {
			newData[key] = value;
		}
	}
	return newData;
};

const writeTokenFile = (name, data) =>
	writeFileSync(`./tokens/${name}.json`, JSON.stringify(data, null, `\t`));

export const rewriteRawTokens = (theme, rawTokens, layers) => {
	for (const layer of layers) {
		const tokens = rawTokens[layer][theme];
		writeTokenFile(`${theme}/${layer}`, tokens);
	}

	// // writeTokenFile(`${theme}/semantic`, tokens["semantic"]);
	// writeTokenFile(
	// 	`${theme}/components`,
	// 	removePrefix(tokens["components"], "semantic"),
	// );
};
