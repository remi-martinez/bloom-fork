const roundWithDecimals = (value, decimalPlaces = 2) => {
	// exit if value is undefined
	if (value === undefined) {
		return;
	}
	// check for correct inputs
	if (typeof value !== "number" || typeof decimalPlaces !== "number") {
		throw new Error(
			`Invalid parameters, both value "${value}" (${typeof value}) and decimalPlaces "${decimalPlaces}" (${typeof decimalPlaces}) must be of type number`
		);
	}
	// set decimal places
	const factorOfTen = Math.pow(10, decimalPlaces);
	// round result and return
	return Math.round(value * factorOfTen) / factorOfTen;
};

export const roundRgba = (rgba) => ({
	r: roundWithDecimals(rgba.r * 255, 0),
	g: roundWithDecimals(rgba.g * 255, 0),
	b: roundWithDecimals(rgba.b * 255, 0),
	a: roundWithDecimals(rgba.a ?? 1),
});

export const convertRgbaObjectToString = (rgbaObject) =>
	`rgba(${rgbaObject.r}, ${rgbaObject.g}, ${rgbaObject.b}, ${rgbaObject.a})`;
