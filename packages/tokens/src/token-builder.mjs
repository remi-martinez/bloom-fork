import StyleDictionary from "style-dictionary";
import { convertRgbaObjectToString, roundRgba } from "./colors.mjs";

const transforms = [
	"attribute/cti",
	"color/css",
	"content/icon",
	"custom/color",
	"custom/pixel",
	"name/cti/kebab",
	"size/rem",
	"time/seconds",
];

/** @returns {StyleDictionary.Config} */
const buildConfigFor = (theme, layers, buildConfig) => ({
	include: layers.map((layer) => `tokens/${theme}/${layer}.json`),
	platforms: {
		css: {
			transforms,
			transformGroup: "css",
			buildPath: `${buildConfig.outputDirectory}/${theme}/`,
			files: [
				{
					format: "css/variables",
					destination: "tokens.css",
					options: {
						selector: `.${theme}`,
						outputReferences: true,
					},
				},
			],
		},
		scss: {
			transforms: [...transforms, "font/quote"],
			transformGroup: "scss",
			buildPath: `${buildConfig.outputDirectory}/${theme}/`,
			files: [
				{
					format: "scss/map-flat",
					destination: "tokens.scss",
					options: {
						outputReferences: true,
					},
				},
			],
		},
	},
});

StyleDictionary.registerTransform({
	name: "custom/color",
	type: "value",
	matcher: ({ original: { value } }) =>
		typeof value === "object" && "r" in value && "g" in value && "b" in value,
	transformer: ({ original: { value } }) =>
		convertRgbaObjectToString(roundRgba(value)),
});

StyleDictionary.registerTransform({
	name: "custom/pixel",
	type: "value",
	matcher: ({ original: { value }, path }) => path.indexOf('weight') === -1 && typeof value === "number",
	transformer: ({ original: { value } }) => `${value / 16}rem`,
});

StyleDictionary.registerTransform({
	name: "font/quote",
	type: "value",
	matcher: ({ original, attributes }) =>
		typeof original.value === "string" && attributes?.category === "font",
	transformer: ({ original: { value } }) => `'${value}'`,
});

export const buildCssVariablesFor = (theme, layers, buildConfig) =>
	StyleDictionary.extend(
		buildConfigFor(theme, layers, buildConfig),
	).buildAllPlatforms();
