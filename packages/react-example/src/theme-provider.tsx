import "@bloom-kit/tokens/loccitane/tokens.css";
import { FC } from "react";

export const ThemeProvider: FC<{
	theme: "loccitane";
	children: React.ReactNode;
}> = ({ theme, children }) => <div className={theme}>{children}</div>;
