import styled from "styled-components";
import { createComponent } from "@lit/react";

import { ThemeProvider } from "../theme-provider";
import React from "react";

const StyledApp = styled.div`
	// Your style here
`;

export const Button = createComponent({
	tagName: "b-button",
	elementClass: HTMLElement,
	react: React,
	events: {
		onclick: "click",
	},
});

export const Card = createComponent({
	tagName: "b-card",
	elementClass: HTMLElement,
	react: React,
});

export function App() {
	return (
		<StyledApp>
			<ThemeProvider theme="loccitane">
				<Button onClick={() => console.log("clicked")}>test</Button>
				<Card>Content</Card>
			</ThemeProvider>
		</StyledApp>
	);
}

export default App;
