# @bloom-kit/components

## 0.1.0-next.29

### Patch Changes

- Accessibility

## 0.1.0-next.28

### Patch Changes

- Fix for touch on mobile

## 0.1.0-next.27

### Patch Changes

- Change to set click externally

## 0.1.0-next.26

### Patch Changes

- Rename property
- Updated dependencies
  - @bloom-kit/tokens@0.1.0-next.14

## 0.1.0-next.25

### Patch Changes

- Feedback refacto

## 0.1.0-next.24

### Patch Changes

- Add accessibility on wishlist

## 0.1.0-next.23

### Patch Changes

- Wishlist
- Updated dependencies
  - @bloom-kit/tokens@0.1.0-next.13

## 0.1.0-next.22

### Patch Changes

- Add justify content to local template

## 0.1.0-next.21

### Patch Changes

- Change rem size to adapt to SFCC side

## 0.1.0-next.20

### Patch Changes

- Add Feedback Message to index.ts

## 0.1.0-next.19

### Patch Changes

- c512484: Change style to adapt to new component ui

## 0.1.0-next.18

### Patch Changes

- 84dbc94: Add Feedback Message Component
- Updated dependencies [84dbc94]
  - @bloom-kit/tokens@0.1.0-next.12

## 0.1.0-next.17

### Patch Changes

- Color changes
- Updated dependencies
  - @bloom-kit/tokens@0.1.0-next.11

## 0.1.0-next.16

### Patch Changes

- Updated dependencies [c26a609]
  - @bloom-kit/tokens@0.1.0-next.10

## 0.1.0-next.15

### Patch Changes

- button and card improvements
- Updated dependencies
  - @bloom-kit/tokens@0.1.0-next.9

## 0.1.0-next.14

### Patch Changes

- b964e67: feat(tokens): add all tertiary button tokens
- Updated dependencies [b964e67]
  - @bloom-kit/tokens@0.1.0-next.8

## 0.1.0-next.13

### Patch Changes

- 2e12975: fix(components): card remove inherit all

## 0.1.0-next.12

### Patch Changes

- fd7a4e8: fix(components): button sizes, card link property inheritance
- Updated dependencies [fd7a4e8]
  - @bloom-kit/tokens@0.1.0-next.7

## 0.1.0-next.11

### Patch Changes

- d17974a: fix(components): typo in tertiary
- Updated dependencies [d17974a]
  - @bloom-kit/tokens@0.1.0-next.6

## 0.1.0-next.10

### Patch Changes

- cb42307: feat(components): add terciary button
- Updated dependencies [cb42307]
  - @bloom-kit/tokens@0.1.0-next.5

## 0.1.0-next.9

### Patch Changes

- c70b46a: card link inherit properties

## 0.1.0-next.8

### Patch Changes

- 6839fcf: introduce card-width token

## 0.1.0-next.7

### Patch Changes

- fe62c1d: add a component css reset

## 0.1.0-next.6

### Patch Changes

- 1f7338b: Make link card full height

## 0.1.0-next.5

### Patch Changes

- 9d6b2a7: fix link card, make whole content clickable

## 0.1.0-next.4

### Patch Changes

- b72434b: add clickable/focusable link card
- Updated dependencies [b72434b]
  - @bloom-kit/tokens@0.1.0-next.4

## 0.1.0-next.3

### Minor Changes

- ae70cf8: add "font-weight" token

### Patch Changes

- Updated dependencies [ae70cf8]
  - @bloom-kit/tokens@0.1.0-next.3

## 0.0.1-next.2

### Patch Changes

- Updated dependencies [117967e]
  - @bloom-kit/tokens@0.0.1-next.2

## 0.0.1-next.1

### Patch Changes

- Updated dependencies [6e67417]
  - @bloom-kit/tokens@0.0.1-next.1

## 0.0.1-next.0

### Patch Changes

- 5dfdbb9: Initial test release
- Updated dependencies [5dfdbb9]
  - @bloom-kit/tokens@0.0.1-next.0
