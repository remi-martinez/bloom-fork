import { css } from "lit";

export const focusRing = css`
	outline: 1px dashed var(--button-border-color);
	outline-offset: 2px;
	outline: dashed black;
`;

export const cssReset = css`
	:host * {
		box-sizing: border-box;
	}
`;