import { LitElement, css, html, unsafeCSS, nothing } from "lit";
import { customElement, property } from "lit/decorators.js";
import { focusRing } from "../css";

const styleFor = (variant: ButtonVariant) => {
	const content = `
		--button-bg-disabled: var(--button-${variant}-bg-disabled);
		--button-bg-hover: var(--button-${variant}-bg-hover);
		--button-bg: var(--button-${variant}-bg-default);
		--button-border-disabled: var(--button-${variant}-border-disabled);
		--button-border-hover: var(--button-${variant}-border-hover);
		--button-border-width-disabled: var(--button-${variant}-border-width-disabled);
		--button-border-width: var(--button-${variant}-border-width-default, 1px);
		--button-border: var(--button-${variant}-border-default);
		--button-text-disabled: var(--button-${variant}-text-disabled);
		--button-text-hover: var(--button-${variant}-text-hover);
		--button-text: var(--button-${variant}-text-default);
	`;

	return unsafeCSS(content);
};

type ButtonVariant = "primary" | "secondary" | "tertiary";

@customElement("b-button")
export class BloomButton extends LitElement {
	static override styles = css`
		:host {
			display: inline-flex;
			cursor: pointer;
			${styleFor("primary")}
		}

		:host([variant="secondary"]) {
			${styleFor("secondary")}
		}

		:host([variant="tertiary"]) {
			${styleFor("tertiary")}
		}

		:host([disabled]) {
			--button-bg: var(--button-bg-disabled);
			--button-text: var(--button-text-disabled);
			--button-border: var(--button-border-disabled);
		}

		.button {
			-webkit-appearance: none;
			align-items: center;
			background-color: var(--button-bg);
			border-color: var(--button-border);
			border-radius: var(--button-radius);
			border-width: var(--button-border-width);
			border-style: solid;
			box-sizing: border-box;
			color: var(--button-text);
			cursor: inherit;
			display: inline-flex;
			font-family: var(--font-family-sans);
			font-size: var(--font-size-ui);
			font-weight: var(--font-weight-ui);
			gap: inherit;
			height: 100%;
			justify-content: center;
			line-height: var(--font-line-height-ui);
			min-width: 64px;
			outline: none;
			padding: 0;
			padding: var(--button-padding-block) var(--button-padding-inline);
			text-decoration: none;
			vertical-align: middle;
			width: 100%;

			&::-moz-focus-inner {
				padding: 0;
				border: 0;
			}
		}

		.button:disabled {
			cursor: initial;
		}

		.button:hover:not([disabled]) {
			background-color: var(--button-bg-hover);
			color: var(--button-text-hover);
		}

		.button:focus-visible {
			${focusRing}
		}

		.button:active:not([disabled]) {
			filter: brightness(95%);
		}
	`;

	@property()
	variant: ButtonVariant = "primary";

	@property({
		converter: {
			fromAttribute: (value) => value === "" || value === "true",
		},
	})
	disabled = false;

	/**
	 * The URL that the link button points to.
	 */
	@property() href = "";

	/**
	 * Where to display the linked `href` URL for a link button. Common options
	 * include `_blank` to open in a new tab.
	 */
	@property() target: "_blank" | "_parent" | "_self" | "_top" | "" = "";

	// TODO: Add aria attributes support
	// TODO: Add form element support
	// Adapt from: https://github.dev/material-components/material-web

	protected override render() {
		const buttonOrLink = this.href ? this.renderLink() : this.renderButton();
		return html`${buttonOrLink}`;
	}

	private renderLink() {
		return html`<a
			href=${this.href}
			target=${this.target || nothing}
			class="button"
			?disabled=${this.disabled}
		>
			<slot />
		</a>`;
	}

	private renderButton() {
		return html`<button class="button" ?disabled=${this.disabled}>
			<slot />
		</button>`;
	}
}

declare global {
	interface HTMLElementTagNameMap {
		"b-button": BloomButton;
	}
}
