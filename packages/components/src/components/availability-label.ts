import { LitElement, css, html } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement("b-availability-label")
export class BloomAvailabilityLabel extends LitElement {
	static override styles = css`
		:host {
			--availability-label-dot-color: var(--color-icon-success, #3b7902);
			align-items: center;
			color: var(--color-text, #001022);
			display: flex;
			gap: 8px;
			font-size: var(--font-size-body-s, 12px);
			line-height: var(--font-line-height-body-s, 1rem);
			font-family: var(--font-family-sans, LOccitaneSans, Inter);
		}

		:host([status="low-inventory"]) {
			--availability-label-dot-color: var(--color-icon-warning, #af4d23);
		}

		:host([status="out-of-stock"]) {
			--availability-label-dot-color: var(--color-icon-error, #d0021b);
		}

		:host > .dot {
			background-color: var(--availability-label-dot-color, #3b7902);
			border-radius: 8px;
			height: 8px;
			width: 8px;
		}
	`;

	@property()
	status: "available" | "low-inventory" | "out-of-stock" = "available";

	protected override render() {
		return html`
			<i class="dot"></i>
			<span class="body-s"><slot /></span>
		`;
	}
}

declare global {
	interface HTMLElementTagNameMap {
		"b-availability-label": BloomAvailabilityLabel;
	}
}
