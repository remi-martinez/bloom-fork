import { LitElement, css, html, unsafeCSS } from "lit";
import { customElement, property } from "lit/decorators.js";
import errorIconM from '../assets/icons/error-icon-m.svg?raw';
import successIconM from '../assets/icons/success-icon-m.svg?raw';
import warningIconM from '../assets/icons/warning-icon-m.svg?raw';
import infoIconM from '../assets/icons/info-icon-m.svg?raw';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';

const styleFor = (variant: Status) => {
	const content = `
        --background-color: var(--color-bg-${variant});
        --border-color: var(--color-border-${variant});
        --text-color: var(--color-text-default);
`;

	return unsafeCSS(content);
};

type Template = "local" | "page";
type Status = "success" | "warning" | "info" | "error";

const statusIcons : Record<Status, string> = {
    success: successIconM,
    warning: warningIconM,
    info: infoIconM,
    error: errorIconM,
};

@customElement("b-feedback-message")
export class BloomFeedbackMessage extends LitElement {
    static override styles = css`
        :host {
            font-family: var(--font-family-sans, LOccitaneSans, Inter);
        }

        :host([status="warning"][template="local"]) {
            --text-color: var(--color-text-warning, #A3532F);
        }

        :host([status="success"][template="local"]) {
            --text-color: var(--color-text-success, #3B7902);
        }

        :host([status="error"][template="local"]) {
            --text-color: var(--color-text-error, #D0021B);
        }

        :host([status="info"][template="local"]) {
            --text-color: var(--color-text-info, #3067C5);
        }

        :host([template="local"]) {
            align-items: center;
            display: flex;
            justify-content: center;
            gap: 0.8rem;
        }

        :host([template="page"]) {
            align-items: center;
            display: flex;
            gap: 0.8rem;
            background-color: var(--background-color, transparent);
            border: 0.1rem solid var(--border-color, transparent);
            border-radius: var(--feedback-message-radius, 0);
            padding: 1.6rem;
        }

        :host([status="warning"][template="page"]) {
            ${styleFor('warning')}
        }

        :host([status="success"][template="page"]) {
            ${styleFor('success')}
        }

        :host([status="error"][template="page"]) {
            ${styleFor('error')}
        }

        :host([status="info"][template="page"]) {
            ${styleFor('info')}
        }

        .icon {
            display: var(--icon-display, block);
            fill: var(--text-color);
        }

        .body-m {
            color: var(--text-color);
            font-size: var(--font-size-ui);
            line-height: var(--font-line-height-ui);
            font-weight: var(--font-weight-ui);
        }

        .icon {
            width: 1.6rem;
            height: 1.6rem;
        }
    `;

    @property({ reflect: true })
    status: Status = "success";

    @property({ reflect: true })
    template: Template = "local";

    protected override render() {
        return html`
            <span class="icon">${unsafeSVG(statusIcons[this.status])}</span>
            <span class="body-m"><slot /></span>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "b-feedback-message": BloomFeedbackMessage;
    }
}
