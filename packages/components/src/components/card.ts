import { LitElement, css, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { cssReset, focusRing } from "../css";

const containerStyle = css`
	background-color: var(--color-bg-surface, white);
	border-color: var(--color-border-brand);
	border-radius: var(--card-radius, 0);
	border-style: solid;
	border-width: var(--card-border-width, 1px);
	color: var(--color-text, #001022);
	display: block;
	font-family: var(--font-family-sans, LoccitaneSans, Inter, sans-serif);
	font-size: var(--font-size-body-s, 12px);
	line-height: var(--font-line-height-body-s, 1rem);
	padding: var(--card-padding, var(--space-200) var(--space-200) var(--space-100));
`;

@customElement("b-card")
export class BloomCard extends LitElement {
	static override styles = css`
		${cssReset}
		:host(:not([href])) {
			${containerStyle}
		}
		:host([href]) > a {
			${containerStyle}
			color: unset;
			cursor: pointer;
			height: 100%;
			outline: unset;
			text-decoration: unset;
			width: inherit;
			max-width: inherit;
			margin: inherit;
		}
		:host([href]:hover) > a {
			border-color: var(--color-border-brand-hover);
			background-color: var(--color-bg-surface-hover);
		}
		:host([href]:focus-within) {
			${focusRing}
		}
	`;

	@property({ reflect: true })
	href?: string;

	protected override render() {
		if (!this.href) {
			return html` <slot /> `;
		}
		return html` <a .href=${this.href}>
			<div>
				<slot />
			</div>
		</a>`;
	}
}

declare global {
	interface HTMLElementTagNameMap {
		"b-card": BloomCard;
	}
}
