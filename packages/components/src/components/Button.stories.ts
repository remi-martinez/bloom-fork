// Button.stories.ts

import type { Meta, StoryObj } from "@storybook/web-components";
import "./button";

import { html } from "lit";

const meta: Meta = {
	component: "b-button",
};

export default meta;
type Story = StoryObj;

export const Primary: Story = {
	render: () => html` <b-button>Do an action</b-button> `,
};

export const Secondary: Story = {
	render: () => html` <b-button variant="secondary">Do an action</b-button> `,
};

export const Tertiary: Story = {
	render: () => html` <b-button variant="tertiary">Do an action</b-button> `,
};

export const Disabled: Story = {
	render: () => html` <b-button disabled>Do an action</b-button> `,
};

export const LinkButton: Story = {
	render: () => html` <b-button href="/" target="_blank">Do an action</b-button> `,
}