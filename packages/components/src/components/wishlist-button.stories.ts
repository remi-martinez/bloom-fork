// Button.stories.ts

import type { Meta, StoryObj } from "@storybook/web-components";
import "./wishlist-button";
import { html } from "lit";

const meta: Meta = {
	component: "b-wishlist-button",
};

export default meta;
type Story = StoryObj;

export const WishlistAllTemplates: Story = {
	render: () => html`<b-wishlist-button></b-wishlist-button>
	<b-wishlist-button template="menu"></b-wishlist-button>`,
};

export const AddToWishlistButton: Story = {
	render: () => html`<b-wishlist-button clicked></b-wishlist-button>

	<b-wishlist-button ></b-wishlist-button>`,
};

export const WishlistMenuIcon: Story = {
	render: () => html`<b-wishlist-button template="menu"></b-wishlist-button>`,
};