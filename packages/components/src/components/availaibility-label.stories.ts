import type { Meta, StoryObj } from "@storybook/web-components";
import "./availability-label";

import { html } from "lit";

const meta: Meta = {
	component: "b-availability-label",
};

export default meta;
type Story = StoryObj;

export const All: Story = {
	render: () => html`
		<div>
			<b-availability-label>Available</b-availability-label>
			<b-availability-label status="low-inventory">
				Low inventory
			</b-availability-label>
			<b-availability-label status="out-of-stock">
				Out Of Stock
			</b-availability-label>
		</div>
	`,
};

export const Available: Story = {
	render: () => html` <b-availability-label>Available</b-availability-label> `,
};

export const LowInventory: Story = {
	render: () =>
		html`
			<b-availability-label status="low-inventory">
				Low inventory
			</b-availability-label>
		`,
};

export const OutOfStock: Story = {
	render: () =>
		html`
			<b-availability-label status="out-of-stock">
				Out Of Stock
			</b-availability-label>
		`,
};
