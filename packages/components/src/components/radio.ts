import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { isActivationClick } from "../events";
import { polyfillElementInternalsAria } from "../aria";

import { SingleSelectionController } from "../single-selection-controller.js";

const CHECKED = Symbol("checked");

@customElement("b-radio")
export class BloomRadio extends LitElement {
	static formAssociated = true;

	/**
	 * Whether or not the radio is selected.
	 */
	@property({ type: Boolean })
	get checked() {
		return this[CHECKED];
	}
	set checked(checked: boolean) {
		const wasChecked = this.checked;
		if (wasChecked === checked) {
			return;
		}

		this[CHECKED] = checked;
		const state = String(checked);
		this.internals.setFormValue(this.checked ? this.value : null, state);
		this.requestUpdate("checked", wasChecked);
		this.selectionController.handleCheckedChange();
	}

	[CHECKED] = false;

	/**
	 * Whether or not the radio is disabled.
	 */
	@property({ type: Boolean, reflect: true }) disabled = false;

	/**
	 * The element value to use in form submission when checked.
	 */
	@property() value = "on";

	/**
	 * The HTML name to use in form submission.
	 */
	get name() {
		return this.getAttribute("name") ?? "";
	}
	set name(name: string) {
		this.setAttribute("name", name);
	}

	/**
	 * The associated form element with which this element's value will submit.
	 */
	get form() {
		return this.internals.form;
	}

	/**
	 * The labels this element is associated with.
	 */
	get labels() {
		return this.internals.labels;
	}

	private readonly selectionController = new SingleSelectionController(this);
	private readonly internals = polyfillElementInternalsAria(
		this,
		(this as HTMLElement) /* needed for closure */
			.attachInternals()
	);

	constructor() {
		super();
		this.addController(this.selectionController);
		this.internals.role = "radio";
		this.addEventListener("click", this.handleClick.bind(this));
		this.addEventListener("keydown", this.handleKeydown.bind(this));
	}

	protected override render() {
		return html`
			<input
				id="input"
				type="radio"
				tabindex="-1"
				.checked=${this.checked}
				.value=${this.value}
				?disabled=${this.disabled}
			/>
			<label for="input"><slot /></label>
		`;
	}

	protected override updated() {
		this.internals.ariaChecked = String(this.checked);
	}

	private async handleClick(event: Event) {
		if (this.disabled) {
			return;
		}

		// allow event to propagate to user code after a microtask.
		await 0;
		if (event.defaultPrevented) {
			return;
		}

		if (isActivationClick(event)) {
			this.focus();
		}

		// Per spec, clicking on a radio input always selects it.
		this.checked = true;
		this.dispatchEvent(new Event("change", { bubbles: true }));
		this.dispatchEvent(
			new InputEvent("input", { bubbles: true, composed: true })
		);
	}

	private async handleKeydown(event: KeyboardEvent) {
		// allow event to propagate to user code after a microtask.
		await 0;
		if (event.key !== " " || event.defaultPrevented) {
			return;
		}

		this.click();
	}

	/** @private */
	formResetCallback() {
		// The checked property does not reflect, so the original attribute set by
		// the user is used to determine the default value.
		this.checked = this.hasAttribute("checked");
	}

	/** @private */
	formStateRestoreCallback(state: string) {
		this.checked = state === "true";
	}
}

declare global {
	interface HTMLElementTagNameMap {
		"b-radio": BloomRadio;
	}
}
