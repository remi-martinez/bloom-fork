import type { Meta, StoryObj } from "@storybook/web-components";
import "./feedback-message";
import { html } from "lit";

const meta: Meta = {
    component: "b-feedback-message",
};

export default meta;
type Story = StoryObj;

const statuses = ['success', 'warning', 'error', 'info'];
const templates = ['page', 'local'];

const createFeedbackMessageStory = (status: string, template: string) => {
    return html`
        <b-feedback-message status="${status}" template="${template}">
            ${status.charAt(0).toUpperCase() + status.slice(1)} ${template} - Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </b-feedback-message>
    `;
};

export const AllCombinations: Story = {
    render: () => html`
        <div style="display: flex; flex-direction: column; gap: 10px;">
            ${statuses.flatMap(status =>
                templates.map(function(template) {
                    return createFeedbackMessageStory(status, template);
                }
                )
            )}
        </div>
    `,
};

export const SuccessMedium: Story = {
    render: () => createFeedbackMessageStory('success', 'local'),
};

export const SuccessMediumPage: Story = {
    render: () => createFeedbackMessageStory('success', 'page'),
};

export const WarningMedium: Story = {
    render: () => createFeedbackMessageStory('warning', 'local'),
};

export const WarningMediumPage: Story = {
    render: () => createFeedbackMessageStory('warning', 'page'),
};

export const ErrorMedium: Story = {
    render: () => createFeedbackMessageStory('error', 'local'),
};

export const ErrorMediumPage: Story = {
    render: () => createFeedbackMessageStory('error', 'page'),
};

export const InfoMedium: Story = {
    render: () => createFeedbackMessageStory('info', 'local'),
};

export const InfoMediumPage: Story = {
    render: () => createFeedbackMessageStory('info', 'page'),
};