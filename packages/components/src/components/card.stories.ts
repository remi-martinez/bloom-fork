import type { Meta, StoryObj } from "@storybook/web-components";
import "./card";

import { html } from "lit";

const meta: Meta = {
	component: "b-card",
};

export const Default: StoryObj = {
	render: () => html`
		<b-card style="max-width: 900px">
			<h1>Title</h1>
			<p style="margin-bottom: var(--space-500)">
				And here's a little bit of content that means nothing but has the merit
				to be there.
			</p>
			<div style="display: flex;">
				<b-button style="margin-inline: auto">Acheter</b-button>
			</div>
		</b-card>
	`,
};

export const LinkCard: StoryObj = {
	render: () => html`
		<b-card style="max-width: 900px" href="/">
			<h1>Title</h1>
			<p>
				And here's a little bit of content that means nothing but has the merit
				to be there.
			</p>
		</b-card>
	`,
};

export const LinkCardWidth: StoryObj = {
	render: () => html`
		<b-card style="max-width: 900px; width: 320px;" href="/">
			<h1>Title</h1>
			<p>
				And here's a little bit of content that means nothing but has the merit
				to be there.
			</p>
		</b-card>
	`,
};

export default meta;
