import { LitElement, css, html } from "lit";
import { customElement, property, state } from "lit/decorators.js";
import { classMap } from "lit/directives/class-map.js";
import favoriteIcon from "../assets/icons/favorite-full.svg?raw";
import { unsafeSVG } from "lit/directives/unsafe-svg.js";

type Template = "add" | "menu";

@customElement("b-wishlist-button")
export class BloomWishlistButton extends LitElement {
	static override styles = css`
		:host {
			display: inline-flex;
			justify-content: center;
			align-items: center;
		}

		.wishlist-button {
			display: flex;
			justify-content: center;
			align-items: center;
			width: var(--wishlist-button-width, 40px);
			height: var(--wishlist-button-width, 40px);
			border: 0.1rem solid black;
			border-radius: var(--wishlist-button-radius, 4px);
			background-color: var(--wishlist-button-bg-default, #fffefa);
			transition: background-color 0.3s, border 0.3s;
			cursor: pointer;
		}

		.wishlist-button svg {
			width: var(--wishlist-button-icon-width, 24px);
			transition: transform 0.3s;
		}

		.wishlist-button svg path {
			fill: white;
			stroke: var(--wishlist-button-icon-stroke, #001022);
			transition: fill 0.3s;
		}

		.wishlist-button.hover svg path {
			fill: var(--wishlist-button-icon-fill-hover, #ffdb4c);
		}

		:host([clicked]) .wishlist-button {
			background-color: var(--wishlist-button-bg-selected, #fff1b4);
			border-color: transparent;
		}

		:host([clicked]) .wishlist-button svg path {
			fill: var(--wishlist-button-icon-fill-selected, #ffcb00);
		}

		.wishlist-button.bump svg {
			animation: bump 0.3s;
		}

		@keyframes bump {
			0% {
				transform: scale(1);
			}
			50% {
				transform: scale(0.9);
			}
			100% {
				transform: scale(1);
			}
		}

		:host([template="menu"]) .wishlist-button {
			border: none;
			background-color: transparent;
		}

		:host([template="menu"]) .wishlist-button svg path {
			fill: var(--wishlist-button-icon-fill-selected, #ffcb00);
		}

		:host([template="menu"]) .wishlist-button svg {
			filter: drop-shadow(0px 3px 3px rgba(0, 0, 0, 0.5));
			animation: menu-animation 3s infinite;
		}

		@keyframes menu-animation {
			0% {
				transform: rotate(0deg) scale(1);
			}
			5% {
				transform: rotate(10deg) scale(1);
			}
			10% {
				transform: rotate(0deg) scale(1);
			}
			15% {
				transform: rotate(-10deg) scale(1);
			}
			20% {
				transform: rotate(0deg) scale(1.1);
			}
			25% {
				transform: rotate(0deg) scale(1);
			}
			100% {
				transform: rotate(0deg) scale(1);
			}
		}
	`;

	@state()
	private bump = false;

	@state()
	private isHovered = false;

	@property({ type: Boolean, reflect: true })
	clicked = false;

	@property({ type: String })
	template: Template = "add";

	protected override render() {
		const favoriteIconWithAttributes = favoriteIcon.replace(
			"<svg",
			'<svg aria-hidden="true" focusable="false"',
		);

		const classes = {
			bump: this.bump,
			hover: !this.clicked && this.isHovered,
		};

		return html`
			<button
				type="button"
				aria-label="${this.template === "add" ? "Add to wishlist" : "Wishlist"}"
				aria-pressed="${this.clicked ? "true" : "false"}"
				class="wishlist-button ${classMap(classes)}"
				@click="${this.toggleClicked}"
				@mouseenter="${this.addHoverEffect}"
				@mouseleave="${this.removeHoverEffect}"
				@touchstart="${this.removeHoverEffect}"
			>
				${unsafeSVG(favoriteIconWithAttributes)}
			</button>
		`;
	}

	private toggleClicked() {
		this.clicked = !this.clicked;
		this.bump = true;
		this.removeHoverEffect();
		setTimeout(() => (this.bump = false), 300);
	}

	private addHoverEffect() {
		if (!this.clicked) {
			this.isHovered = true;
			this.requestUpdate();
		}
	}

	private removeHoverEffect() {
		this.isHovered = false;
		this.requestUpdate();
	}

	setClickedState(isClicked: boolean) {
		this.clicked = isClicked;
		this.removeHoverEffect();
	}
}

declare global {
	interface HTMLElementTagNameMap {
		"b-wishlist-button": BloomWishlistButton;
	}
}
