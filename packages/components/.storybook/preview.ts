import { Decorator, Preview } from "@storybook/web-components";
import { html } from "lit";
import { StyleInfo, styleMap } from "lit/directives/style-map.js";

import "@bloom-kit/tokens/erborian/tokens.css";
import "@bloom-kit/tokens/melvita/tokens.css";
import "@bloom-kit/tokens/loccitane/tokens.css";
import "@bloom-kit/tokens/erborian-us/tokens.css";
import "../index.scss";

const themes = [
	{
		value: "loccitane",
		icon: "circle",
		title: "L'occitane",
	},
	{
		value: "erborian",
		icon: "circle",
		title: "Erborian",
	},
	{
		value: "erborian-us",
		icon: "circle",
		title: "Erborian US",
	},
	{
		value: "melvita",
		icon: "circle",
		title: "Melvita",
	},
];

type Theme = (typeof themes)[number];

export const globalTypes = {
	theme: {
		name: "Theme",
		description: "Branding theme",
		defaultValue: themes[0].value,
		toolbar: {
			// The icon for the toolbar item
			icon: "paintbrush",
			// Array of options
			items: [
				...themes,
				{
					value: "side-by-side",
					icon: "sidebyside",
					title: "Side by side",
				},
			],
			// Property that specifies if the name of the item will be displayed
			showName: true,
		},
	},
};

const withTheme = (StoryFn: () => unknown, { value, title }: Theme) => html`
	<div class="${value}" id="theme-provider">
		<div
			class="label"
			style=${styleMap({
				fontFamily: "var(--font-family-sans)",
				fontSize: "var(--font-size-body-s)",
			})}
		>
			${title}
		</div>
		${StoryFn()}
	</div>
`;

const findOrThrow = <T>(array: T[], predicate: (arg: T) => boolean) => {
	const value = array.find(predicate);
	if (!value) {
		throw new Error(`No value found in array`);
	}
	return value;
};

const theming: Decorator = (StoryFn, context) => {
	const { theme } = context.globals;
	return html`
		<style>
			.label {
				background: var(--color-grey-100);
				border-top-right-radius: var(--radius-100);
				bottom: 0;
				color: var(--color-grey-300);
				font-family: var(--font-family-sans);
				font-size: var(--font-size-body-s);
				left: 0;
				padding: var(--space-025) var(--space-100);
				position: absolute;
			}
			div.grid {
				display: grid;
				margin: -16px;
				height: 100vh;
				justify-items: center;
				grid-template-columns: repeat(auto-fit, minmax(256px, auto));
			}
			div.grid > * {
				display: flex;
				position: relative;
				border-right: solid 1px var(--color-grey-200);
				border-bottom: solid 1px var(--color-grey-200);
				width: 100%;
				padding: 32px;
				justify-content: space-around;
				box-sizing: border-box;
			}
			div.grid > * > * {
				height: min-content;
			}
		</style>
		${theme === "side-by-side"
			? html`<div class="grid">
					${themes.map((_theme) => withTheme(StoryFn, _theme))}
				</div>`
			: html`${withTheme(
					StoryFn,
					findOrThrow(themes, (t) => t.value === theme),
				)}`}
	`;
};

const preview: Preview = {
	parameters: {},
	decorators: [theming],
};

export default preview;
