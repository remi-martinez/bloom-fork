---
"@bloom-kit/components": patch
"@bloom-kit/tokens": patch
---

fix(components): button sizes, card link property inheritance
